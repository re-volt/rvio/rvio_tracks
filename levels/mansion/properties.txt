{

PICKUPS {
  SpawnCount      14 2                           ; Initial and per-player count
  EnvColor        255 255 128                   ; Color of shininess (RGB)
  LightColor      128 96 0                      ; Color of light (RGB)
}

MATERIAL {
ID              1
Name            "Wet Floor" ; replaced marble
Skid            false
Spark           false
Corrugated      false
Moves           false
Dusty           true
Roughness       0.5
Grip            0.1
Hardness        1
DefaultSound    92 
ScrapeSound	    92
SkidSound 	    92
CorrugationType 1
DustType        1
SkidColor       255 255 255
}
	
DUST {
ID              1
Name            "Water particles" ; replaced gravel
SparkType       3
ParticleChance  5
ParticleRandom  0
SmokeType       0
SmokeChance     0
SmokeRandom     0
}
	
SPARK {
ID              3
Name            "Water particles" ; replaced popcorn
CollideWorld    false
CollideObject   false
CollideCam      false
HasTrail        false
FieldAffect     false
Spins           true
Grows           true
Additive        true
Horizontal      false
Size            20 20
UV              0 0.5
UVSize          0.25 0.25
TexturePage     47
Color           150 150 150
Mass            0.05
Resistance      0.002
Friction        0
Restitution     0
LifeTime        1
LifeTimeVar     0
SpinRate        80
SpinRateVar     10
SizeVar         10
GrowRate        0
GrowRateVar     36
}

MATERIAL {
ID              2
Name            "Wooden floor" ; replaced stone
Skid            true
Spark           false
Corrugated      true
Moves           false
Dusty           false
Roughness       1
Grip            1
Hardness        1
ScrapeSound	    5
SkidSound 	    44
CorrugationType 2
DustType        2
SkidColor       200 200 200
}

CORRUGATION {
ID              2
Name            "Wooden floor"
Amplitude       2
Wavelength      10 100
}

MATERIAL {
  ID              6                             ; Material ID [0 - 63]
  Name            "Geen Carpet"                  ; replaced carpettile
  Color           170 20 0                      ; Display color

  Skid            true                         ; Skidmarks appear on material
  Spark           true                         ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                         ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            0.700000                      ; Grip of the material
  Hardness        0.000000                      ; Hardness of the material

  DefaultSound    7                            ; Sound when driving
  SkidSound       7                             ; Sound when skidding
  ScrapeSound     7                             ; Car body scrape [5:Normal]

  SkidColor       164 198 165                      ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness
  DustType        3                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

DUST {
  ID              3                             ; Dust ID [0 - 31]
  Name            "Carpet Pieces"                       ; Display name

  SparkType       6                             ; Particle to emit
  ParticleChance  0.01000                      ; Probability of a particle
  ParticleRandom  0.001000                      ; Probability variance

  SmokeType       27                            ; Smoke particle to emit
  SmokeChance     0.010000                      ; Probability of a smoke part.
  SmokeRandom     0.000000                      ; Probability variance
}

MATERIAL {
  ID              15                             ; Material ID [0 - 63]
  Name            "Paper"                     ; Display name
  Color           153 153 153                   ; Display color

  Skid            true                          ; Skidmarks appear on material
  Spark           false                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            1.000000                      ; Grip of the material
  Hardness        1.000000                      ; Hardness of the material

  DefaultSound    72                            ; Sound when driving
  SkidSound       72                             ; Sound when skidding
  ScrapeSound     72                             ; Car body scrape [5:Normal]

  SkidColor       0 0 0                   ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}
SPARK {
  ID              8                             ; Particle ID [0 - 63]
  Name            "WATER"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        false                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            10.000000 10.000000           ; Size of the particle
  UV              0.750000 0.062500             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           150 150 200                     ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.001000                      ; Air resistance
  Friction        0.800000                      ; Sliding friction
  Restitution     0.050000                      ; Bounciness

  LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     1.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         0.000000                      ; Size variation
  GrowRate        0.000000                      ; How quickly it grows
  GrowRateVar     0.000000                      ; Grow variation

  TrailType       -1                            ; ID of the trail to use
}

MATERIAL {
  ID              10                             ; Material ID [0 - 63]
  Name            "Bedding"                  ; replaced ice1
  Color           170 20 0                      ; Display color

  Skid            true                         ; Skidmarks appear on material
  Spark           false                         ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       0.500000                      ; Roughness of the material
  Grip            2.500000                      ; Grip of the material
  Hardness        0.000000                      ; Hardness of the material

  DefaultSound    7                            ; Sound when driving
  SkidSound       7                             ; Sound when skidding
  ScrapeSound     7                             ; Car body scrape [5:Normal]

  SkidColor       200 200 200                      ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness
  DustType        3                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}
}