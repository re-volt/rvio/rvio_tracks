___________________________________________________________________
:			Radioactive Garden		 	  :
:いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�:
:			Common information			  :
:			┬┬┬┬|┬┬┬┬┬�			  :
:Author:			|			Urnemanden:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Length:			|			389 meters:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Assumed difficulity:		|			    Medium:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Theme:		    		|	       Garden crossed with: 
:				|	radioactivty; Surrealistic:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Amount of polygons:		|		   22465 triangles:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:_______________________________�_________________________________:
:			  Tools & kits used		  	  :
:			  ┬┬┬|┬┬┬┬┬�	          	  :
:2D tools in use:		|			 Paint.NET:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:3D tools in use:		|			 3ds max 8:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Kits in use, before		|	       JimK's offroad kit,:
:3ds max importing:	   	|		RickyD's track kit:
:				|	       & the warehouse kit:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Other tools:			|			  Rvtmod-7:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Time spended:			|			3-4 months:
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�|┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:_______________________________�_________________________________:
:			   Description				  :
:			   ┬┬┬┬┬�				  :
:After heading towards North for a long time, from the quite odd  :
:jungle road, the small group found by late evening a forest path :
:leading them directly to a garden with the most eccentric and    :
:exotic plants they've seen. Discovering all the boxes of         :
:radioactive fluid gave them a leading clue to where in the world :
:they've come, and now they were trying to get away from the more :
:or less mutated vegetation before the probably even more crazy   :
:person behind all this finds them there.			  :
:								  :
:Radioactive Garden is my 8th release, originally created using   :
:the "magic" MAKEITGOOD mode. But for 3-4 months ago I decided to :
:try import it into 3ds max and basically finish and upload it as :
:an unfinished track of mine.Not long after I started, I entered  :
:a mood, that made me quite creative, which resulted in making    :
:the project turn out much better than I would ever think it 	  :
:would (compared to the basis I had at start).			  :
:								  :
:_________________________________________________________________:
:		Texture information & copyright			  :
:		┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�
|'''|Contains a softly modified stonefloor originally from 	  :
| A |RickyD's track kit. The green foliage is for small plants	  :
|___|such as flower leaves and the plants in the small greenhouse.:
|					   			  :
|'''|Contains 4 different 128x128 textures. 3 out of 4 is 100%	  :
| B |original, while the 4th is under Killer Wheel's and	  :
|___|Darksabre's copyright (permission confirmed).     		  :
|					    			  :
|'''|All textures here has been softly modified, but I choosed    :
| C |to leave the original structure as in the warehouse kit.	  :
|___|The skeleton of the big greenhouse is made by JimK too.	  :
|					   		  	  :
|'''|The texture that indicates how the windows in both		  :
| D |greenhouses look. Made by me in the past using MS Paint.	  :
|___|								  :
|					    			  :
|'''|Earth textures, with a tiny bit of space left. Everything	  :
| E |there is my work, and goes under my copyright (read at 	  :
|___|bottom) like the rest of the work of mine.			  :
|					   			  :
|'''|A render of one of one of the trees inside the track. Used	  :
| F |For extraordinary 2D foliage and billboards in areas where	  :
|___|the racing line is in distance.			  	  :
|					    			  :
|'''|All plant textures, some made by me, some originally from 	  :
| G |Darksabre's & Killer Wheel's track, CTR Palm Marsh. 	  :
|___|(Permission was given)					  :
|					    			  :
|'''|This branch was probably the texture that took most time for :
| H |me to create. A huge amount of the leaves is unique to each  :
|___|other, but most background leaves is copy-off's.    	  :
|					      			  :
|'''|Approximately 70% of this stock-structured bitmap is original:
| I |while you might be able to recognize the usual plant textures:
|___|just a bit modified (The bush is actually my own work too).  :
|					      			  :
|'''|Contains the forest path texture, an alternative branch 	  :
| J |texture and the grass texture used mostly in the outside part:
|___|of the garden. Everything is 100% original.	          :
|					      			  :
|'''|An environmental texture, to add shine towards a few 	  :
|ENV|instances in this track and towards the "ball.m".		  :
|___|100% original.						  :
:_________________________________________________________________:
:			     Credit				  :
:			     ┬┬┬				  :
:THANKS:							  :
:┬┬┬	Aeon - for helping me understand and modify the AI Nodes. :
:	Gaming4JC - For helping me with the triggers.		  :
:	Hilare9 - For giving the triggers the finishing touch 	  :
:	Killer Wheels - For texture-permission and for feedback.  :
:	MOH - For offering help with the triggers and cheering.   :
:	Zach - For taking over Oilthing, so I could focus on RAG. :
:	Santa - For giving me a opportunity to release bfore xmas!:
:	Lord Ash - For Feedback at #Game-artist.net		  :
:   Everyone else whom I have problems remembering the names of.  :
:_________________________________________________________________:
:			Contact Information			  :
:			┬┬┬┬┬┬┬┬┬�			  :
:E-mail:		Hougaard.junior@gmail.com		  :
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Alternative E-mail:	Webmaster@urnemanden.com		  :
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:Website:		http://www.urnemanden.com		  :
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:_________________________________________________________________:
:		       Copyright & Permission			  :
:		       ┬┬┬┬┬┬┬┬┬┬┬			  :
:You MAY use textures & 3D geometry if credits was given 	  :
:to me or. Permission from Killer Wheels     			  :
:and/or Darksabre is needed if you wish to borrow their	  	  :
:textures too. 							  :
:Please do NOT use this track as a part of a product		  :
:unless it's free for everyone and unless this readme.txt haven't :
:been modified in any way. I recommend that you contact me 	  :
:promptly if needed.						  :
:_________________________________________________________________:
:			     Feedback				  :
:			     ┬┬┬┬				  :
:Any feedback is welcome, but you should expect a reply back, if  :
:bad critique doesn't have base. The reason is that I find your   :
:opinion very important - I wish to improve.			  :
:_________________________________________________________________:
:			Thanks for reading!			  :
:┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬�:
:いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい�:
:vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv: