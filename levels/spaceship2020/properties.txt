SPARK {
  ID              31                             ; Particle to replace [0 - 30]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 75 0                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        2.100000                      ; Sliding friction
  Restitution     1.500000                      ; Bounciness

  LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     2.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         3.000000                      ; Size variation
  GrowRate        3.000000                      ; How quickly it grows
  GrowRateVar     3.100000                      ; Grow variation

  TrailType       0                           ; ID of the trail to use
}


TRAIL {
  ID              0                             ; Trail to replace [0 - 2]
  Name            "FIREWORK"                    ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands 
  UV              0.90 0.60           ; UV coord for trail segments
  UVEnd           0.90 0.60             ; UV coord for last segment
  UVSize          0.001 0.001             ; Width and height of both UV
  Color           0 255 75 0               ; Alpha, Red, Green, Blue
  LifeTime        0.300000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          12                            ; Number of segments
  TexturePage     47                            ; Texture page
 
 
}

SPARK {
  ID              32                             ; Particle to replace [0 - 30]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 100 0                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        1.100000                      ; Sliding friction
  Restitution     0.500000                      ; Bounciness

   LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     2.000000                      ; Life time variance

  SpinRate        4.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     1.000000                      ; Variation of the spin rate

  SizeVar         2.000000                      ; Size variation
  GrowRate        0.100000                      ; How quickly it grows
  GrowRateVar     0.100000                      ; Grow variation

  TrailType       1                           ; ID of the trail to use
}


TRAIL {
  ID              1                             ; Trail to replace [0 - 2]
  Name            "FIREWORK"                    ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands 
  UV              0.90 0.60           ; UV coord for trail segments
  UVEnd           0.90 0.60             ; UV coord for last segment
  UVSize          0.001 0.001             ; Width and height of both UV
  Color           0 255 100 0               ; Alpha, Red, Green, Blue
  LifeTime        0.300000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          12                            ; Number of segments
  TexturePage     47                            ; Texture page
 
}

SPARK {
  ID              33                             ; Particle to replace [0 - 30]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 125 0                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.500000                      ; Bounciness

  LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     2.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         3.000000                      ; Size variation
  GrowRate        3.000000                      ; How quickly it grows
  GrowRateVar     3.100000                      ; Grow variation

  TrailType       2                           ; ID of the trail to use
}


TRAIL {
  ID              2                             ; Trail to replace [0 - 2]
  Name            "FIREWORK"                    ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands 
  UV              0.90 0.60           ; UV coord for trail segments
  UVEnd           0.90 0.60             ; UV coord for last segment
  UVSize          0.001 0.001             ; Width and height of both UV
  Color           0 255 125 0               ; Alpha, Red, Green, Blue
  LifeTime        0.300000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          12                            ; Number of segments
  TexturePage     47                            ; Texture page
 
 
}

SPARK {
  ID              34                             ; Particle to replace [0 - 30]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 150 0                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.500000                      ; Bounciness

   LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     2.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         3.000000                      ; Size variation
  GrowRate        3.000000                      ; How quickly it grows
  GrowRateVar     3.100000                      ; Grow variation

  TrailType       3                           ; ID of the trail to use
}


TRAIL {
  ID              3                             ; Trail to replace [0 - 2]
  Name            "FIREWORK"                    ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands 
  UV              0.90 0.60           ; UV coord for trail segments
  UVEnd           0.90 0.60             ; UV coord for last segment
  UVSize          0.001 0.001             ; Width and height of both UV
  Color           0 255 150 0               ; Alpha, Red, Green, Blue
  LifeTime        0.300000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          12                            ; Number of segments
  TexturePage     47                            ; Texture page
 
 
}

SPARK {
  ID              35                             ; Particle to replace [0 - 30]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 175 0                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.500000                      ; Bounciness

   LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     2.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         3.000000                      ; Size variation
  GrowRate        3.000000                      ; How quickly it grows
  GrowRateVar     3.100000                      ; Grow variation

  TrailType       4                           ; ID of the trail to use
}


TRAIL {
  ID              4                             ; Trail to replace [0 - 2]
  Name            "FIREWORK"                    ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands 
  UV              0.90 0.60           ; UV coord for trail segments
  UVEnd           0.90 0.60             ; UV coord for last segment
  UVSize          0.001 0.001             ; Width and height of both UV
  Color           0 255 175 0               ; Alpha, Red, Green, Blue
  LifeTime        0.300000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          12                            ; Number of segments
  TexturePage     47                            ; Texture page
 
 
}

SPARK {
  ID              36                             ; Particle to replace [0 - 30]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 200 0                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.500000                      ; Bounciness

   LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     2.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         3.000000                      ; Size variation
  GrowRate        3.000000                      ; How quickly it grows
  GrowRateVar     3.100000                      ; Grow variation

  TrailType       5                           ; ID of the trail to use
}


TRAIL {
  ID              5                             ; Trail to replace [0 - 2]
  Name            "FIREWORK"                    ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands 
  UV              0.90 0.60           ; UV coord for trail segments
  UVEnd           0.90 0.60             ; UV coord for last segment
  UVSize          0.001 0.001             ; Width and height of both UV
  Color           0 255 200 0               ; Alpha, Red, Green, Blue
  LifeTime        0.300000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          12                            ; Number of segments
  TexturePage     47                            ; Texture page
 
 
}

SPARK {
  ID              37                             ; Particle to replace [0 - 30]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 225 0                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.500000                      ; Bounciness

  LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     2.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         3.000000                      ; Size variation
  GrowRate        3.000000                      ; How quickly it grows
  GrowRateVar     3.100000                      ; Grow variation

  TrailType       6                           ; ID of the trail to use
}


TRAIL {
  ID              6                             ; Trail to replace [0 - 2]
  Name            "FIREWORK"                    ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands 
  UV              0.90 0.60           ; UV coord for trail segments
  UVEnd           0.90 0.60             ; UV coord for last segment
  UVSize          0.001 0.001             ; Width and height of both UV
  Color           0 255 225 0               ; Alpha, Red, Green, Blue
  LifeTime        0.300000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          12                            ; Number of segments
  TexturePage     47                            ; Texture page
 
 
}

SPARK {
  ID              38                             ; Particle to replace [0 - 30]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 250 0                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.500000                      ; Bounciness

   LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     2.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         3.000000                      ; Size variation
  GrowRate        3.000000                      ; How quickly it grows
  GrowRateVar     3.100000                      ; Grow variation

  TrailType       7                           ; ID of the trail to use
}


TRAIL {
  ID              7                             ; Trail to replace [0 - 2]
  Name            "FIREWORK"                    ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands 
  UV              0.90 0.60           ; UV coord for trail segments
  UVEnd           0.90 0.60             ; UV coord for last segment
  UVSize          0.001 0.001             ; Width and height of both UV
  Color           0 255 250 0               ; Alpha, Red, Green, Blue
  LifeTime        0.300000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          12                            ; Number of segments
  TexturePage     47                            ; Texture page
 
 
}


SPARK {
  ID              11                            ; Particle to replace [0 - 30]
  Name            "SMOKE TYPE 1 / SIZE 120"                      ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                         ; Collision with camera
  HasTrail        false                        ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            120.000000 120.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           20 19 18                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.001000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        4.400000                      ; Maximum life time
  LifeTimeVar     3.110000                      ; Life time variance

  SpinRate        1.100000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     1.150000                      ; Variation of the spin rate

  SizeVar         1.300000                      ; Size variation
  GrowRate        15.000000                      ; How quickly it grows
  GrowRateVar     16.100000                     ; Grow variation

  TrailType       -1 
}

SPARK {
  ID              12                            ; Particle to replace [0 - 30]
  Name            "SMOKE TYPE 2 / SIZE 60"                      ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                         ; Collision with camera
  HasTrail        false                        ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            60.000000 60.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           25 24 23                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.001000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        3.400000                      ; Maximum life time
  LifeTimeVar     3.110000                      ; Life time variance

  SpinRate        1.200000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     1.350000                      ; Variation of the spin rate

  SizeVar         1.300000                      ; Size variation
  GrowRate        15.000000                      ; How quickly it grows
  GrowRateVar     16.100000                     ; Grow variation

  TrailType       -1 
}

SPARK {
  ID              13                            ; Particle to replace [0 - 30]
  Name            "SMOKE TYPE 3 / SIZE 30"                      ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                         ; Collision with camera
  HasTrail        false                        ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            30.000000 30.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           28 27 26                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.001000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        3.400000                      ; Maximum life time
  LifeTimeVar     3.110000                      ; Life time variance

  SpinRate        1.200000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     1.350000                      ; Variation of the spin rate

  SizeVar         1.300000                      ; Size variation
  GrowRate        15.000000                      ; How quickly it grows
  GrowRateVar     16.100000                     ; Grow variation

  TrailType       -1 
}

SPARK {
  ID              14                            ; Particle to replace [0 - 30]
  Name            "SMOKE1 TYPE4"                      ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      false                         ; Collision with camera
  HasTrail        false                         ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            26.000000 26.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           64 64 64                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.002000                      ; Air resistance
  Friction        0.000000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        2.500000                      ; Maximum life time
  LifeTimeVar     2.100000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     6.000000                      ; Variation of the spin rate

  SizeVar         2.000000                      ; Size variation
  GrowRate        0.000000                      ; How quickly it grows
  GrowRateVar     36.000000                     ; Grow variation

  TrailType       -1                            ; ID of the trail to use
}

SPARK {
  ID              15                            ; Particle to replace [0 - 30]
  Name            "SMOKE TYPE 5 LONG"                      ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      false                         ; Collision with camera
  HasTrail        false                         ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            12.000000 12.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           64 64 64                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.002000                      ; Air resistance
  Friction        0.000000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        5.500000                      ; Maximum life time
  LifeTimeVar     5.100000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     6.000000                      ; Variation of the spin rate

  SizeVar         2.000000                      ; Size variation
  GrowRate        0.000000                      ; How quickly it grows
  GrowRateVar     36.000000                     ; Grow variation

  TrailType       -1                            ; ID of the trail to use
}


SPARK {
  ID              16                            ; Particle to replace [0 - 30]
  Name            "SMOKE TYPE 6 / SIZE 120"                      ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                         ; Collision with camera
  HasTrail        false                        ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            120.000000 120.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           15 14 13                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.001000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        4.400000                      ; Maximum life time
  LifeTimeVar     3.110000                      ; Life time variance

  SpinRate        1.100000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     1.150000                      ; Variation of the spin rate

  SizeVar         1.300000                      ; Size variation
  GrowRate        15.000000                      ; How quickly it grows
  GrowRateVar     16.100000                     ; Grow variation

  TrailType       -1 
}
